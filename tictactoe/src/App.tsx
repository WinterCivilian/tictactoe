import './App.css'
import { useState } from 'react';

function App() {
const [XO, setXO] = useState<string>("X");
const [values, setValues] = useState<string[]>(Array(9).fill(""));
const [winner, setWinner] = useState<string | null>(null);

function Click(index: number){
  if (values[index] !== "" || winner) return; // Prevent changing an already filled cell

  const newValues = [...values];
  newValues[index] = XO;
  setValues(newValues);

      // Check for winner with the updated values
      const foundWinner = checkWinner(newValues);
      if (foundWinner) {
        setWinner(foundWinner);
        console.log(`Winner: ${foundWinner}`);
      } else if (newValues.every(value => value !== "")) {
        setWinner("Draw");
        console.log("Draw");
      } else {
        // Toggle between "X" and "O" if no winner is found
        setXO(prev => (prev === "X" ? "O" : "X"));
      }
}

function checkWinner(values: string[]){
  const winningCombinations = [
    [0, 1, 2], // rows
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6], // columns
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8], // diagonals
    [2, 4, 6]
  ];

    for (const combination of winningCombinations) {
    const [a, b, c] = combination;
    if (values[a] && values[a] === values[b] && values[a] === values[c]) {
      return values[a]; // Return the winner ('X' or 'O')
    }
  }

  return null; // No winner yet
}


function resetGame() {
  setValues(Array(9).fill("")); // Reset all values to empty strings
  setWinner(null); // Reset winner state
  setXO("X"); // Reset the starting player
}

  return (
    <>
      <div className='headline'>
        <h1>{winner ? `Winner: ${winner}` : "Tic Tac Toe"}</h1>
        </div>
      <div className="tictactoe">
      {values.map((value, index) => {
          return (
            <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center', justifyContent: 'center' }}>            
            <button className='mybutton' onClick={() => Click(index)}
                    disabled={!!winner} // Disable button if there's a winner 
              >{value}</button>
            </div>
          );
        })}
      </div>
      <div><button className="reset-button" onClick={resetGame}>Reset Game</button></div>
      
    </>
  )
}


export default App